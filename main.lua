
local IUI = LibStub("LibItemUpgradeInfo-1.0")

local GREY = {0.55, 0.55, 0.55}
local GREEN = {0, 1, 0}
local WHITE = {1, 1, 1}

-- thank you SyLevel!
local function getItemLink(location)
    -- https://www.townlong-yak.com/framexml/27219/EquipmentManager.lua#276
    local player, bank, bags, voidStorage, slot, bag, tab, voidSlot = EquipmentManager_UnpackLocation(location)

    if ( not player and not bank and not bags and not voidStorage ) then
        return
    end

    if not bags then -- player or bank
        return GetInventoryItemLink('player', slot)
    else -- bags
        return GetContainerItemLink(bag, slot)
    end
end

local createText = function(button)
    local label = button.GioFlyout

    if (not label) then
        label = button:CreateFontString(nil,"OVERLAY")
        label:SetFont("Fonts\\FRIZQT__.TTF", 14, "OUTLINE")
        label:SetPoint("CENTER", button, "CENTER", 0, 0)
        button.GioFlyout = label
    end

    return label
end

local function RelativeColor(ilvl)
    --[[local avgItemLevel, avgItemLevelEquipped = GetAverageItemLevel()
    local e = math.floor(avgItemLevelEquipped)

    if ilvl < e then
        return unpack(GREY)
    elseif ilvl > e then
        return unpack(GREEN)
    else
        return unpack(WHITE)
    end]]
    return unpack(WHITE)
end

local function hook_EquipmentFlyout_DisplayButton(button)
    -- https://www.townlong-yak.com/framexml/27219/EquipmentFlyout.lua#370
    local location = button.location

    if (location and location < EQUIPMENTFLYOUT_FIRST_SPECIAL_LOCATION) then
        local link = getItemLink(location)
        local ilvl = IUI:GetUpgradedItemLevel(link)

        local label = createText(button)
        label:SetTextColor(RelativeColor(ilvl))
        label:SetText(ilvl)
        label:Show()
    end

end

hooksecurefunc('EquipmentFlyout_DisplayButton', hook_EquipmentFlyout_DisplayButton)
--IUI:GetUpgradedItemLevel(location)